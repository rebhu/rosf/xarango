# CONTRIBUTING

Versioning structure: X.Y.Z

X -> Major Version

Y -> Mini Version

Z -> Micro Version

## Some tips

1. Do check your tests, code, etc. before commiting. Make sure you are not working on develop branch.
2. Please don't accidentally commit `.gitlab-ci.yml`, `release.sh` and other similar files in the repository.
3. `master` branch only contains release builds, and release candidates. PRs on this branch will be requested again.
4. Read `STYLE-GUIDE.md` for code pattern.

## How to add a feature
    
1. Setup a mirror or a fork of this repo in your account.
2. Make a self explanatory branch. Eg.: `feature/wait-for-sync`, `fix/bug/23`, etc.
3. Add your code, docs, tests, etc.
4. Open a merge request in `master` for `hotfix` branch. And `develop` branch for `feature`, `fix/bug`.
5. Wait for the conversation to get started
6. Voila!! Now we party. XDXD
defmodule Xarango.Mixfile do
  use Mix.Project

  def project do
    [
     app: :xarango,
     version: "0.7.0",
     elixir: "~> 1.7",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps(),
     description: description(),
     package: package()
    ]
  end

  def application do
    [applications: [:logger, :httpoison, :poison]]
  end

  defp description do
    """
    Client library for ArangoDB which uses HTTP for communication
    """
  end

  defp package do
    [
     name: :xarango,
     files: ["lib", "mix.exs", "README*", "LICENSE"],
     maintainers: ["Chintan Mishra"],
    #  maintainers: ["Michel Benevento"],
     licenses: ["Apache License, Version 2.0"],
     links: %{"GitLab" => "https://gitlab.com/Rebhu_Computing/open-source/xarango"}
    ]
  end

  defp deps do
    [
      {:httpoison, "~> 1.0"},
      {:poison, "~> 3.1"},
      {:faker, "~> 0.10", only: :test},
      {:ex_doc, "~> 0.19.0", only: :dev}
    ]
  end
end
